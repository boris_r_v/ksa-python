ksa-python
=====

*Модули расширения дл обработки json-запросов на скриптовом языке Python.*

**Рекомендации:**

    - При создании новых файлов их начинать в больших букв KSA: Например KSA_xxxx.py

**Получение бинарника**

    1. echo "deb http://lin.ksa/std-mpk/deb-repo/ yout_debian_distr main" >> /etc/apt/sources.list
    2. apt-get update
    3. apt-get install ksa-python

**Получение исходников**

    git clone user_name@lin.ksa:/git/ksa-python    

