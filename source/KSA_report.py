# -*- coding: utf-8 -*-

import os, json, sys
import KSA_check_table
def all_sizing_report( json_obj, log_path, project_path ):
    return KSA_check_table.ERROR("Нет обработчика", "Отчет по всем измерениям не реализован (c 10.09.2017)", "KSA_report.py")

def save_check_table( json_obj, log_path, project_path ):
    return KSA_check_table.save("/tmp/check_table.csv", json_obj, log_path, project_path )

def get_report( json_obj, log_path, project_path ):

    #сделаем список обработчиков
    types = {}
    types["full_sizing"] = all_sizing_report
    types["last_sizing"] = save_check_table
    
    #ищем метод обработчик
    handler = types.get( json_obj["request"] )
    if handler:
        return handler(json_obj, log_path, project_path )
	
    return """{"message":"<b>KSA_report.py:<br>Ошибка обработки запроса</b>","mtype":"error","note":"<i>Не найден обработчик для запроса: <b>""" + str(json_obj["request"])+"""</b></i>","type":"message_dialog"}"""

