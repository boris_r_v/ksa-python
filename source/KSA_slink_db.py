
# -*- coding: utf-8 -*-

import os
from sqlobject import *

class ConnWrapper:
    def __init__( self, path ):
	if (path): 
	    self.conn = sqlite.builder() ( path, debug = False )
	    Prices._connection = self.conn		   
	    Tables._connection = self.conn		   
	    TablesLinks._connection = self.conn		   
    	    Version._connection = self.conn		    
    	    Serial._connection = self.conn		    
    	    DevType._connection = self.conn		    
	    Device._connection = self.conn		    
	    Channel._connection = self.conn		    
	    Object._connection = self.conn		    
	    Sw2Uns4i._connection = self.conn		   
	    Rcirc._connection = self.conn		   
	    UniParamsTypes._connection = self.conn		   
	    StdLogGroups._connection = self.conn		   
	    ObjType._connection = self.conn		   
	    Source._connection = self.conn		   
	    Object2Source._connection = self.conn		   

class Serial ( SQLObject ):
    #_connection = get_conn()
    tty = StringCol(unique = True)
    speed = IntCol()
    #devices = MultipleJoin('Device')

class DevType ( SQLObject ):
    #_connection = get_conn()
    name = StringCol()
    meter_inputs = StringCol(default="") #список измериетльных входов разделенный зяпято
    switch_inputs = StringCol(default="") #список коммутипуемых входов разделенный зяпято
    meter_device = BoolCol(default=False) #флаг что это измерительный модуль УНС, УМВ
    switch_device = BoolCol(default=False) #флаг что это модуль коммутируемых измерений, АК, ИОН
    inputs_type = IntCol(default=0) #идентификатор типа входов, см. e_struct.txt
    
class Device (SQLObject):
    #_connection = get_conn()
    name = StringCol(unique = True)
    addr = IntCol()				#Если -1 то это аналоговый канал ТЗК ТС формата МПК
    speed = IntCol()				#Если -1 то это аналоговый канал ТЗК ТС формата МПК
    place = StringCol()
    tty = ForeignKey('Serial', default=None)    	
    dtype = ForeignKey('DevType', default=None)    	
    hasAk3d4 = IntCol(default=-1)

class Sw2Uns4i(SQLObject):
    #_connection = get_conn()
    sw_dev = ForeignKey('Device')    	
    uns_dev = ForeignKey('Device')    	
    channel_code = IntCol()


class Channel(SQLObject):
    #_connection = get_conn()
    device = ForeignKey( 'Device' )    	
    std_object = ForeignKey( 'Object', default = None )    	
    channel = StringCol() 		#имя канала, из поля meter_inputs таблицы DevType, или поле side - для А
    key = IntCol(default=0)		#номер ключа коммутируемого входа, поле key для АК.
    mode = IntCol(default=0)		#режим работы ключа коммутируемого входа, поле mode для АК. Если -1 - то это аналоговый импульс ТЗК ТС
    summary = StringCol( unique = True ) #строка вида dev_id:channel:key:mode - должна быть уникальна, нужна для валидации данных

#Таблица с объектами мнемосхемы к которым привязаны объекты телеизмерений.
class Rcirc(SQLObject):
    #_connection = get_conn()
    name = StringCol()			#Имя объекта мнемсхемы - 2СП
    svg = StringCol()			#svg_id объекта мнемосхемы - svg2@g1245
    mnemo = StringCol()			#Тип объеткта мнемохсемы - Секция, Сигнал, Путь

class UniParamsTypes(SQLObject):
    #_connection = get_conn()
    name = StringCol()			#Имя типа измеряемой величины, Путевое реле, Фильтр и т.д.
    unit = StringCol()			#Единица измерений величины, вольты(В), амперы(А), по системе СИ
    isActive = BoolCol(default=True)   	#Флаг того, что данный параметр используется в данном проекта
    isNumeric = BoolCol(default=False)  #Флаг того, что это цифровой параметр

class StdLogGroups(SQLObject):
    #_connection = get_conn()
    name = StringCol()			#Имя Группы протокола "Рельсовые цепи", "Светофоры" и т.д.
    isActive = BoolCol(default=True)   	#Флаг того, что данный параметр используется в данном проекте

class Prices(SQLObject):
    #_connection = get_conn()
    dtype = StringCol()    	#Иденитфикатор типа усройства
    price = FloatCol()	
    

class Version(SQLObject):
    #_connection = get_conn()
    data = StringCol()				#Таблица с версией базы данных
						#id=1 версия БД
						#id=2 имя станции

class Source(SQLObject):
    #_connection = get_conn()
    name = StringCol( unique = True )		#имя измеряемого параметра ОТИ
    vector_index = IntCol( )			#индекс массива получаемого от УНС4и где лежит данное измерение
    text_index = StringCol( unique = True )   	#Тестовый индек параметра VDC,VAC,VACf, ISOL, VA, ...
    isActive = BoolCol( default = True )   	#учавствует в данном проекте?
    unit = StringCol( default = "В" )		#единица измерения значения	

class Object2Source(SQLObject):
    #_connection = get_conn()
    object = ForeignKey("Object")		#идентификатор объекта телеизмерений
    source = ForeignKey("Source")		#идентификатор измеряемого параметра
    umax = FloatCol( default = 0.0 )            #максимально допустимое значение
    umin = FloatCol( default = 0.0 )		#минимально допустимое значение
    uoff = FloatCol( default = 0.0 )		#значенеи при выключеном объекте
    alfa = FloatCol( default = 0.0 )		#множитель
    betta = FloatCol( default = 0.0 )		#смещение
    state_sence = IntCol( default = 0 )		#если 1 то измерение зависит от сосояния объекта мнемосхемы, или тут может быть идентификатор канала, для измерений получаемых по каналу ТС

class ObjType(SQLObject):
    #_connection = get_conn()
    name = StringCol()			#имя типа объекта диагностики
    title = StringCol()			#описание типа объекта диагностики


class Object(SQLObject):	#объет коммутируемых измерений
    #_connection = get_conn()
    channel = ForeignKey('Channel', default=None )    	#идентификатор измерительного канала системы ТИ
    name = StringCol( unique = True )           	#имя объекта диагностики
    log_group = ForeignKey( 'StdLogGroups', default=1 ) #группа для архива
    title = ForeignKey( 'UniParamsTypes', default=1 )  	#пояснение к объекту диагностики
    objtype = ForeignKey( 'ObjType' )			#Тип объекта диагностики (коммутируемый, УНСПА, Параметры кода)
    rcirc = ForeignKey('Rcirc', default=1)    		#Элемент мнемосхемы объекта 
	#параметры коммутируемых объектов диагностики
    no_dim_isol = IntCol(default=0)      		#флаг, не измерять изоляцию
    no_dim_volt = IntCol(default=0)      		#флаг, не измерять напряжение
    current_dim = IntCol(default=0)      		#флаг, измерение тока на шунте, или если для объекта УМВ тип входа. см.w_link.py строка 146
    hand_dim_isol = BoolCol( default = True )		#флаг измерения изоляции только в ручном режиме. 
    more_title = StringCol(default="")   		#дополнительное пояснение к объекту диагностики
    imin = FloatCol( default = 0.0 )			#минимально допустимое значение изоляции
    term = StringCol(default="")   			#условия внеочередного измерения(для коммутирумеых) или условие фиксации данных измерения(для композитных)
    ts_term = StringCol(default="")   			#вспомогательное поле к условию, - сюда пишем условие для конеесктора дискретного ТС
	#параметры объектов УНС-ПА - 
	#устареваеть и как передаляю УНС-ПА на композитные измерения - это удалить
	#Устаревший - не используется для установки порога изоляции, но содержит различные флаги
	#Если ka значение == -1 - то это флаг, что условие (term) это выражение ТС
    ka = FloatCol( default = 0.0 )			#коэффициент пересчета напряжения с датчика Холла в ток для фазы А
    kb = FloatCol( default = 0.0 )			#коэффициент пересчета напряжения с датчика Холла в ток для фазы B
    kc = FloatCol( default = 0.0 )                      #коэффициент пересчета напряжения с датчика Холла в ток для фазы C
    umax = FloatCol( default = 0.0 )                    #максимально допустимое значение
    umin = FloatCol( default = 0.0 )			#минимально допустимое значение


class Tables(SQLObject):	#таблица со списком таблиц увязок
    #_connection = get_conn()
    name = StringCol( unique = True )           #имя канала
    type = StringCol( )      			#тип таблицы, [serial-apk, ethernet-apk]
    data = MultipleJoin("TablesLinks")		#Список данных данной таблицы	
    reserv1 = StringCol( )      		#Reserved field
    reserv2 = StringCol( )      		#Reserved field
    

class TablesLinks(SQLObject):	#таблица с наполненеим таблиц увязок
    #_connection = get_conn()
    table = ForeignKey( 'Tables' )			
    object = ForeignKey("Object")		#идентификатор объекта телеизмерений
    source = ForeignKey("Source")		#идентификатор измеряемого параметра
    number = IntCol( default = -1 )      		#номер измерения в таблице ТС (увязка по RS-485) или группе (для увязки по 62130-22TP)
    grp = IntCol( default = -1 )      		#номер группы измерений в таблице(для увязки по 62130-22TP)
