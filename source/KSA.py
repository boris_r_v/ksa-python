# -*- coding: utf-8 -*-

import os,sys
import json
import KSA_report
#ret = json.loads( '{"message":"<b>Первое сообщение от Python</b>","mtype":"error","note":"<i>И это круто</i>","type":"message_dialog"}' )
#json.dumps( ret )
#json_obj = json.loads( json_req )

def ksa_get_report( json_obj, log_path, project_path ):
    return KSA_report.get_report( json_obj, log_path, project_path )

def ksa_input_point( json_req, log_path, project_path ):
    print "Python json-request: ", json_req
    print "Python log_path: ", log_path
    print "Python project_path: ", project_path
    print "Python work dir: ", os.getcwd()

    try:    
	#сделаем список обработчиков
	types = {}
        types["report_req"] = ksa_get_report # same as: KSA_report.get_report
        #сделаем json object
        json_obj = json.loads( json_req )
        #ищем метод обработчик
        handler = types.get( json_obj["type"] )
        if handler:
		return handler(json_obj, log_path, project_path )
        return """{"message":"<b>KSA.py:<br>Ошибка обработки запроса</b>","mtype":"error","note":"<i>Не найден обработчик для запроса типа: <b>""" + str(json_obj["type"])+"""</b></i>","type":"message_dialog"}"""

    except:
	print sys.exc_info()
	return KSA_report.KSA_check_table.ERROR( "Ошибка обработки запроса", str(sys.exc_info() ), "KSA.py" )


