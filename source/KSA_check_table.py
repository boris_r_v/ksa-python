# -*- coding: utf-8 -*-

import os, sys, json
import KSA_slink_db
import sqlite3
from collections import defaultdict

default_data_t = lambda: defaultdict(default_data_t)

ssence = {-1:u"Не важно", 1:u"Занята", 0:u"Свободна"}
last_sizing = "last.sqlite.db"

def save_error_to_file( title_, str_ ):
    p = "~/ksa_error.txt"
    print p
    ffile = open( p, "w")
    ffile.write ( str_ )
    ffile.close()

def ERROR( title_,  str_, prefix_="KSA_check_table.py" ):
    str_ = str_.replace("<", "(", 100 )
    str_ = str_.replace(">", ")", 100 )
    str_ = str_.replace("[", "(", 100 )
    str_ = str_.replace("]", ")", 100 )
    str_ = str_.replace("\'", "", 100 )
    str_ = str_.replace("\"", "", 100 )
    return """{"message":"<b>"""+prefix_+""":<br>"""+ title_+ """</b>","mtype":"error","note":"<i>""" + str_+"""</i>","type":"message_dialog"}"""

def OK( str_ ):
    return """{"message":"<b>Отчет сформирован</b>","mtype":"info","note":"<a href=""" + str_+ """ download >Скачать</a>","type":"message_dialog"}"""

class Table_Object():
    def __init__( self, _mnemo_name, _log_group_name, _title_name, _obj_name ):
	
	self.mnemo_name = _mnemo_name
	self.log_group_name = _log_group_name
	self.title_name = _title_name
	self.obj_name = _obj_name
	self.data = default_data_t()
	self.meter_place = ""
	self.meter_input = ""

    def __repr__( self ):
	return "Table_Object: "+ self.obj_name + " " + self.title_name

def create_sizing_obj_list( project_path ):
    KSA_slink_db.ConnWrapper( project_path )
    l = {}
    for obj in KSA_slink_db.Object.select():
	to = Table_Object( obj.rcirc.name, obj.log_group.name, obj.title.name, obj.name )
	mi = ""
	if ( obj.channel ):
	    to.meter_place = obj.channel.device.place
	    mi = "Uвх"+obj.channel.channel+"."+str(obj.channel.key)
	    if ( obj.channel.mode ):
		if( 1 == obj.channel.mode ): 
		    mi = mi +" верхний в паре" 
		if( 2 == obj.channel.mode ): 
		    mi = mi +" нижний в паре" 
	to.meter_input = mi
	key = unicode(obj.name, 'utf-8' ) #READ: http://stackoverflow.com/questions/4182603/python-how-to-convert-a-string-to-utf-8
	l[key] = to
    return l

def set_sizing_value( sl, log_path ):
    conn = sqlite3.connect( log_path +"/"+ last_sizing )
    cur = conn.cursor()    

    #Read Algo sizing descr
    for row in cur.execute ("select * from algo_sizing"):
	iname = row[1].encode('utf8') #READ: http://stackoverflow.com/questions/6048085/writing-unicode-text-to-a-text-file
	mnemo_name = row[4].encode('utf8')
	log_group_name = row[3].encode('utf8')
	title_name = row[2].encode('utf8')
	to = Table_Object( mnemo_name, log_group_name, title_name, iname )
	#print mnemo_name, log_group_name, title_name, iname
	#print type(mnemo_name), type(log_group_name), type(title_name), type(iname)
	sl[row[1]] = to

    #Read sizing data
    for row in cur.execute ("select * from last_sizing"):
        #[iname][source][mnemo_state] = value	
        if row[0] in sl.keys():
	    sl[unicode(row[0])].data[unicode(row[1])][ssence[row[4]]] = row[2] 
	#else:
	    #print "NOT found iname:\""+ unicode(row[0])+"\""
    return sl

def save( path, json_obj, log_path, project_path ):
    try:
	ffile = open( path, "w")
	ffile.write( "Объект мнемосхемы,Группа объектво ТИ,Тип объекта ТИ,Имя,Источник,Состояние РЦ, Значение,Изм.модуль,Изм.вход.модуля\n" )

    except:
	print sys.exc_info()
	return ERROR( "Ошибка создания файла отчета: "+ path, str(sys.exc_info() ) )

    try:
	sl = create_sizing_obj_list( project_path )
    except:
	print sys.exc_info()
	return ERROR( "Ошибка чтения базы адаптации: " + project_path, str(sys.exc_info() ) )

    try:
	sll = set_sizing_value( sl, log_path )
    except:
	print sys.exc_info()
	return ERROR( "Ошибка чтения архива измерений: " + log_path, str(sys.exc_info() ) )

    try:
	for key in sll:
	    obj = sll[key]
	    for src in obj.data:
		for ms in obj.data[src]:
		    #print "**", obj.mnemo_name, obj.log_group_name, obj.title_name, obj.obj_name, src, ms, obj.data[src][ms]
		    #print "**", type(obj.mnemo_name), type(obj.log_group_name), type(obj.title_name), type(obj.obj_name), type(src), type(ms), type(obj.data[src][ms])	
		    #READ: http://stackoverflow.com/questions/6048085/writing-unicode-text-to-a-text-file
		    ffile.write ( "%s,%s,%s,%s,%s,%s,%f,%s,%s\n" % (  obj.mnemo_name,  obj.log_group_name, obj.title_name, obj.obj_name, src.encode('utf8'), ms.encode('utf8'), obj.data[src][ms], obj.meter_place, obj.meter_input ) )
	ffile.close()    	
        return OK( path )
    except:
	print sys.exc_info()
	return ERROR( "Ошибка обработки данных при создании отчета",  str(sys.exc_info() ) )


