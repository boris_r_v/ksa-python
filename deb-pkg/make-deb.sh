#!/bin/bash

DIR=./ksa-python
VER=`grep Version: $DIR/DEBIAN/control | cut -c9-30`
ARC=`grep Architecture: $DIR/DEBIAN/control | cut -c14-30`

echo "compile python source"
python -m compileall ../source/*.py

echo "copy python-bin"
cp -v ../source/*.pyc ./ksa-python/usr/lib/python2.7/dist-packages/
rm -f ../source/*.pyc

echo "create deb-package"
rm -f ksa-python-$VER.$ARC.deb
fakeroot dpkg-deb --build $DIR

echo "Rename $DIR.deb to  ksa-python-$VER.$ARC.deb"
mv  $DIR.deb ksa-python-$VER.$ARC.deb
